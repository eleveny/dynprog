#include <iostream>
#include <vector>

using namespace std;

unsigned long fibR(unsigned n)//recursive fibonacci implementation
{
  
  //...
  if (n == 1 || n == 2)
	  return 1;
  else
	  return fibR(n-1) + fibR(n-2);	
  
}



unsigned long fibDP(unsigned n)//dynamic programming fibonacci implementation
{
  
  //... 
 int Fib[n+1];

 Fib[0]=0;
 Fib[1]=1;

 for(unsigned i=2;i<=n;i++)
	 Fib[i]=Fib[i-1]+Fib[i-2];

 return Fib[n];

}


int main()
{

  unsigned input;

  
   cerr<<"Welcome to \"Fibonacci Comparison Program\". We first need some input :  ";

    cin>>input;

    cerr<<endl<<"fibDP("<<input<<") =  "<<fibDP(input)<<endl;

    cerr<<endl<<"fibR("<<input<<") =  "<<fibR(input)<<endl;

    return 0;
}
